{-|
   Module      : Parse
   Description : De functies die te maken hebben met het verwerken van ruwe data
   Copyright   : (c) Brian van der Bijl, 2019-2020
                     License     : BSD
                     Maintainer  : brian.vanderbijl@hu.nl
                     Stability   : experimental
                     Portability : POSIX
 -}

module Parse where

import Common (readWithDefault)

-- | Splits een lijst (string) op basis van een element --- voor ons doel de komma in een csv
splitOn :: Eq a => a -> [a] -> [[a]]
splitOn x l =
  case l of
    []          -> []
    e:es | e==x -> split es
    es          -> split es
  where
    split es = let (first,rest) = break (x==) es
               in first : splitOn x rest

-- | Splits een lijst op 70%, om training- en testdata te scheiden
splitTrainTest :: [a] -> ([a],[a])
splitTrainTest samples = splitAt cutoff samples
  where size = length samples
        cutoff = ceiling $ 0.7 * fromIntegral size
