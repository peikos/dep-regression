{-|
   Module      : Common
   Description : Algemene (structurele) helper-functies
   Copyright   : (c) Brian van der Bijl, 2019-2020
                     License     : BSD
                     Maintainer  : brian.vanderbijl@hu.nl
                     Stability   : experimental
                     Portability : POSIX
 -}

module Common where

import Data.Maybe (fromMaybe)
import Text.Read (readMaybe)

-- | Read functie met een default value
readWithDefault :: Read a => a -> String -> a
readWithDefault d = fromMaybe d . readMaybe

-- | Voer een functie uit op het eerste element van een tuple
first :: (a -> b) -> (a, c) -> (b, c)
first f (a, c) = (f a, c)

-- | Voer een functie uit op het tweede element van een tuple
second :: (a -> b) -> (c, a) -> (c, b)
second f (c, a) = (c, f a)
