{-|
   Module      : Regression
   Description : De functies die te maken hebben met de regressie zelf
   Copyright   : (c) Brian van der Bijl, 2019-2020
                     License     : BSD
                     Maintainer  : brian.vanderbijl@hu.nl
                     Stability   : experimental
                     Portability : POSIX
 -}

module Regression where

import Datatypes

import Common (first)
import Data.List (findIndex)

-- | Initiele waarde voor theta is de nulvector van lengte 11 (10 features plus bias)
emptyTheta :: Coefficients
emptyTheta = replicate 11 0

-- | Hulpfunctie om leesbaarheid te vergroten bij cost
half :: Fractional n => n -> n
half x = x / 2

-- | Bereken het gemiddelde van een lijst van getallen
average :: (Num n, Fractional n) => [n] -> n
average nums = sum nums / fromIntegral (length nums)

-- | Bereken het inwendig / dot product van twee vectoren (voorgesteld door lijsten van getallen)
dot :: Num n => [n] -> [n] -> n
dot a = sum . zipWith (*) a

-- | Hypothesefunctie, geeft op basis van de huidige theta een voorspelling van de y die bij een voorbeeld x hoort. Komt neer op het inwendig product van de vector theta en de vector x (voorafgegaan van een 1 voor de bias).
hypothesis :: Coefficients  -- ^ De huidige waarde voor theta
           -> Sample        -- ^ Het datapunt waarvoor de y berekend moet worden
           -> DependentVar  -- ^ De voorspelling op basis van theta en x
hypothesis theta x
  | length theta == length x = dot theta . (1:) $ x
  | otherwise                = error ""

-- | De cost-functie J is de helft van het gemiddelde van alle squared errors.
cost :: TrainingData  -- ^ De totale dataset waarop getraind of getest wordt
     -> Coefficients  -- ^ De huidige waarde voor theta
     -> Cost          -- ^ De totale cost
cost examples theta = half . average . map squaredError $ examples
  where squaredError :: Example -> Error
        squaredError (x,y) = (hypothesis theta x - y)^2

-- | Voor gradient descent wordt de afgeleide van de cost-functie gebruikt. De waarde alpha staat toe de grootte van de stappen te beinvloeden. De afgeleides worden per element van de coefficienten-vector theta berekend; zipWith geeft de index mee, zodat de gevonden error geschaald kan worden met element n van de vector x voor het huidige trainingsvoorbeeld.
gradDescent :: Alpha         -- ^ De stapgroote voor gradient descent
            -> Coefficients  -- ^ De huidige waarde voor theta
            -> TrainingData  -- ^ De gehele trainingsset
            -> Coefficients  -- ^ De uiteindelijke waarde voor theta
gradDescent alpha theta examples = zipWith updateCoefficient [0..] theta
  where updateCoefficient :: Int -> Coefficient -> Coefficient
        updateCoefficient n theta_n = let errors = map error examples
                                      in theta_n - alpha * average errors
          where error :: Example -> Error
                error (x_i, y_i) = (hypothesis theta x_i - y_i) * (x_i !! n)

-- | Train tot de condities bereikt zijn. Hiervoor wordt eerst een oneindige lijst van opeenvolgende waardes voor theta gedefinieerd (maar niet berekend), waarna hier op basis van de stopcondities gezocht wordt naar een geschikt model.
train :: Params                    -- ^ De parameters voor het trainen (alpha, start theta en stopconditie)
      -> TrainingData              -- ^ De gehele trainingsset
      -> Maybe (Int, Coefficients) -- ^ Theta zodra de stopconditie bereikt is
train p@(P theta alpha stop) examples = 
  let repeatedDescent = scanl (gradDescent alpha) theta (repeat examples) -- Als foldl, maar geeft een lijst van tussenliggende waardes.
      c = cost examples
  in case stop of
    -- Hier wordt doorgegaan tot er een waarde is met een totale cost onder de grens
    (UntilCost maxCost) -> do idx <- findIndex ((< maxCost) . c) repeatedDescent
                              return (idx, repeatedDescent !! idx)
    -- Hier worden de eerste twee elementen van de lijst vergeleken; als de tweede minder dan epsilon gedaald is ten opzichte van de eerste is een plateau bereikt. Als de cost toeneemt convergeert het model niet, en moeten de hyperparameters anders worden gekozen.
    (UntilCostChange epsilon) -> findPlateau repeatedDescent 
       where findPlateau :: [Coefficients] -> Maybe (Int, Coefficients)
             findPlateau (r1:r2:rs) | c r2        > c r1     = Nothing
                                    | c r1 - c r2 <= epsilon = Just (0, r2)
                                    | otherwise              = first succ <$> findPlateau (r2:rs)
             findPlateau _ = Nothing -- Een lege lijst zou niet voor moeten komen...
