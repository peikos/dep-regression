{-|
   Module      : Datatypes
   Description : De datatypes en type-synoniemen
   Copyright   : (c) Brian van der Bijl, 2019-2020
                     License     : BSD
                     Maintainer  : brian.vanderbijl@hu.nl
                     Stability   : experimental
                     Portability : POSIX
 -}

module Datatypes where

-- * Type Synonyms om type signatures leesbaarder te maken

type Sample = [Double]
-- ^ Een trainingsvoorbeeld x is een lijst van Doubles

type DependentVar = Double
-- ^ De dependent variable, oftewel de y die we voorspellen

type Coefficient = Double
-- ^ Een element van theta, een weginsfactor voor een feature

type Coefficients = [Coefficient]
-- ^ Theta, de wegingsfactoren van de features

type Example = (Sample, DependentVar)
-- ^ Een trainingsvoorbeeld, zowel x als y

type TrainingData = [Example]
-- ^ De hele training (of test) dataset

type Cost = Double
-- ^ De berekende cost op basis van theta

type Error = Double
-- ^ De berekende afwijking van een coefficient

type Alpha = Double
-- ^ De stapgrootte voor gradient descent

type Epsilon = Double
-- ^ De maximale afname van de cost, waarna het trainen klaar is

-- | De stopconditie, wanneer is gradient descent klaar?
data StopCondition = UntilCostChange Epsilon -- ^ Ga door tot de waarde met minder dan epsilon verandert
                   | UntilCost Cost          -- ^ Ga door tot de totale cost obv de testdata onder een grens komt

-- | Som-type met de parameters voor het trainingsproces
data Params = P { theta_0 :: Coefficients   -- ^ De initele waarde van theta (de nulvector of random)
                , alpha   :: Alpha          -- ^ De stapgroote bij gradient descent
                , stop    :: StopCondition  -- ^ De stopconditie, zie hierboven
                }
