{-|
   Module      : Main
   Description : De daadwerkelijke applicatie (interface) en helper-functies die IO gebruiken
   Copyright   : (c) Brian van der Bijl, 2019-2020
                     License     : BSD
                     Maintainer  : brian.vanderbijl@hu.nl
                     Stability   : experimental
                     Portability : POSIX
 -}

module Main where

import Common (readWithDefault)
import Datatypes
import Parse
import Regression

import System.Random

-- | Vul een theta met random waardes tussen 0 en n
randomTheta :: Double -> IO Coefficients
randomTheta max = do stdGen <- getStdGen
                     return . map (*max) . take 11 . randoms $ stdGen

-- | Quick 'n dirty functie om een CSV in te lezen en te parsen
readCSV :: String -> IO TrainingData
readCSV filename = map (splitXY . map read . splitOn ',') . tail . lines <$> readFile filename
                   where splitXY row = (init row, last row)
                      
-- | Hier gebeurt het...
main :: IO ()
main = do putStrLn "Enter alpha value [0.0001]? "
          user_alpha <- readWithDefault 0.0001 <$> getLine
          putStrLn "Enter epsilon value [0.1]? "
          user_epsilon <- UntilCostChange . readWithDefault 0.1 <$> getLine
          putStrLn "Choose initial theta; '0' selects the zero-vector, anything else for random."
          init_theta_choice <- getLine
          init_theta <- case init_theta_choice of
            ('0':_) -> return emptyTheta
            _       -> randomTheta 10
          putStrLn "Enter filename of CSV? "
          filename <- getLine
          (trainData, testData) <- splitTrainTest <$> readCSV filename
          let model = train (P init_theta user_alpha user_epsilon) trainData
          case model of
            (Just (_, final_theta)) -> do putStrLn "Final theta: "
                                          print model
                                          putStrLn "Cost when applied to test data:"
                                          print $ cost testData final_theta
            _                       -> putStrLn "Model did not converge"
